' First of all, this comment should be highlighted in a different
' font than the code.
'
' Next, the Function, Class, and Sub declarations should be
' highlighted. Their respective names should also be highlighted, but
' in a different font than the rest of the declaration.
Public Function IsProgramRunning() As Boolean
    ' The name of the variable is in the normal font face, but the
    ' keyword True should be highlighted.
    IsProgramRunning = True
End Function

Public Class HelloWorld
    ' Strings get their own font face.
    Private hello_text As String = "Hello, World!"

    Public Sub DoHello()
        Console.WriteLine(Me.hello_text)
    End Sub
End Class

Sub Main()
    ' If you press <Tab> on the following lines, that line
    ' should be auto-indented to eight spaces (the visual-basic-mode
    ' default).
    Dim ipr As Boolean = IsProgramRunning()
    Dim hw As New HelloWorld()

    ' However, if you press <Tab> on the next line, it should be
    ' indented to four spaces to match the line above it (i.e. this
    ' comment).
  hw.DoHello()
End Sub
