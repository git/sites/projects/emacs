List of licenses for example files. Format of entries is:

category/package
    filename            license         origin/comment

------------------------------------------------------

app-emacs/auctex
    circ.tex            GPL-3+          AUCTeX

app-emacs/autoconf-mode
    aclocal.m4          permissive      aclocal

app-emacs/cldoc
    cldoc-test.lisp     public-domain   trivial

app-emacs/cperl-mode
    example.pl          GPL-1+/Artistic Archive-Zip

app-emacs/csharp-mode
    test.cs             public-domain   trivial

app-emacs/csv-mode
    test.csv            public-domain   trivial

app-emacs/develock
    ChangeLog           permissive      GNU Emacs

app-emacs/doxymacs
    use_flag.h          GPL-2           Gatt

app-emacs/dts-mode
    p1025rdb.dtsi       BSD || GPL-2+   Linux kernel (arch/powerpc/boot/dts)
    p1025rdb_32b.dts    BSD || GPL-2+   Linux kernel (arch/powerpc/boot/dts)

app-emacs/flashcard
    emacs.deck          public-domain   trivial

app-emacs/graphviz-dot-mode
    helloworld.dot      public-domain   trivial

app-emacs/haskell-mode
    Interp2.lhs         approx. MIT     http://www.haskell.org/haskellwiki/File:Interp2.lhs

app-emacs/initsplit
    dotemacs.initsplit  public-domain   trivial

app-emacs/jam-mode
    strawberry.jam      public-domain   trivial

app-emacs/jasmin
    hello.j             CC-BY-SA-3.0    http://en.wikipedia.org/wiki/Jasmin_(software)

app-emacs/javascript
    lcm.js              CC-BY-SA-3.0    http://en.wikipedia.org/wiki/JavaScript

app-emacs/lua-mode
    alt_getopt.lua      MIT             TeXlive

app-emacs/matlab
    test.m              public-domain   trivial

app-emacs/mmm-mode
    mmm-example.sh      Texinfo-manual  mmm-mode

app-emacs/mode-compile
    hello.c             public-domain   trivial

app-emacs/muse
    test.muse           public-domain   trivial

app-emacs/nagios-mode
    test_suite.cfg      GPL-3+          nagios-mode

app-emacs/nxml-mode
    test.valid.xml      public-domain   trivial
    test.invalid.xml    public-domain   trivial

app-emacs/ocaml-mode, app-emacs/tuareg-mode
    text.ml             LGPL-2+         OCaml

app-emacs/org-mode
    example.org         FDL-1.3+        org-mode

app-emacs/php-mode
    admin.php           GPL-2           GAMMU

app-emacs/po-mode
    cups_de.po          GPL-2           CUPS

app-emacs/psql
    test.sql            public-domain   trivial

app-emacs/rst
    example.rst         public-domain   trivial

app-emacs/ruby-mode
    biorhythm.rb        BSD-2           Ruby

app-emacs/sml-mode
    test.sml            public-domain   trivial

app-emacs/teco
    example.teco        unknown         99-bottles-of-beer.net

app-emacs/vhdl-mode
    example.vhdl        CC-BY-SA-3.0    http://en.wikipedia.org/wiki/VHDL

app-emacs/visual-basic-mode
    Hello.vb            public-domain   https://bugs.gentoo.org/506866#c7

app-emacs/xrdb-mode
    Emacs.ad            public-domain   trivial

app-emacs/xslide
    test.xsl            public-domain   trivial
