#! /bin/sh
#   One of the long-time standard syntaxes for outputting large amounts
#of code (or text, or HTML, or whatever) from a script (notably shell
#scripts and Perl scripts) is the here-document syntax:

cat <<END_HTML;
<html>
  <head>
    <title>Test Page</title>
  </head>
  <body>
    A test.
  </body>
</html>
END_HTML

#   The 'here-doc' submode class recognizes this syntax, and can even
#guess the correct submode to use in many cases.  For instance, it would
#put the above example in `html-mode', noticing the string `HTML' in the
#name of the here-document.  If you use less than evocative
#here-document names, or if the submode is recognized incorrectly for
#any other reason, you can tell it explicitly what submode to use.

cat <<END_LISP
;; String to integer conversion for arbitrary base.
(defun string-to-base-int (s base)
  "Convert S to an integer by parsing it as base BASE number."
  (let ((n 0))
    (while (not (string-equal s ""))
      (let ((c (downcase (aref s 0))))
	(setq n (+ (* n base)
		   (cond ((and (>= c ?0) (<= c ?9))
			  (- c ?0))
			 ((and (>= c ?a) (<= c ?z))
			  (- c (- ?a 10)))))))
      (setq s (substring s 1)))
    n))
END_LISP

# Local Variables:
# eval: (mmm-ify-by-class 'here-doc)
# End:
